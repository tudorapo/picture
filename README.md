# Picture handler tools

I have a pi which takes pictures from the street at every few minutes.

These scripts are to show them in a web browser and to create a movie from them.

## How to restore after an Event:

1. git clone this, of course.
1. Follow [this](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04) writeup to set up the venv and stuff
1. nginx config snippet:
   ```
   location = /cam { rewrite ^ /cam/; }
   location /cam { try_files $uri @cam; }
   location @cam {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        include uwsgi_params;
        uwsgi_pass unix:/home/pi/picture/showpicture.sock;
   }
   ```
1. Snippet for proxying through another nginx:
   ```
   location /vaciut/ {
          proxy_pass http://bigmac/cam/;
          proxy_set_header Host $host;
          sub_filter_once off;
          sub_filter 'gw/cam' 'gw/vaciut';
   }
   ``` 
1. Snippet for proxying to apache 2.4:
   ```
   <VirtualHost IP:PORT>
      [skipped common stuff]
      # because nginx compresses? no idea.
      SetOutputFilter INFLATE;proxy-html;DEFLATE
      ProxyHTMLEnable On
      #By default no html entity is replaced, we have to tell.
      ProxyHTMLLinks a href
      ProxyHTMLLinks img src
      #For debugging.
      #LogLevel info proxy_html:trace8
      <Location /vaciut>
         ProxyPass "http://192.168.3.2/vaciut"
         ProxyPassReverse "http://192.168.3.2/vaciut"
         #The script is using "cam" and display all links like that, this is 
         #why we have to swap it. The R is necessary.
         ProxyHTMLURLMap cam vaciut R
         #This is to use our hostname and not the one of the target machine.
         ProxyPreserveHost On
      </Location>
   </VirtualHost>
   ```

