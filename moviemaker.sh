#!/bin/dash
PICDIR="/opt/cam/kep"
VIDDIR="/opt/cam/video"
if test -z "$1" ; then
        export DATE=$(date -d '1 day ago' +%-m%d)
    else
            export DATE="$1"
fi
echo $DATE
find "$PICDIR" -name "*vaci-$DATE*jpg" -printf 'file %p\n' | sort -n > "$PICDIR"/list-"$DATE".txt
ffmpeg -y -loglevel panic -threads 0 -r 60 -safe 0 -f concat -i "$PICDIR"/list-"$DATE".txt  -c:v libx264 \
    -vf "fps=60,format=yuv420p"  -vf scale=-1:1080 "$PICDIR"/video-"$DATE".mp4

